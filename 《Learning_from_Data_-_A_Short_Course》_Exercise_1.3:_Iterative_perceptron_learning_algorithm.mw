
== Metadata ==
{{problem
|problem ID=20150106-iterative-perceptron-learning-algorithm
|name=Iterative perceptron learning algorithm
|book ID=learning-from-data
|ISBN-13=978-1600490064
|page=8
|problem index=Exercise 1.3
|classification=[[:Category:Computer_Science|Computer Science]]`[[:Category:Machine_Learning|Machine Learning]]
}}

== Background ==

== Prerequisite ==

== Involved concepts ==

* perceptron

* learning algorithm

* learning model (see 《Learning from Data ﹣ A Short Course》P5)

== Problem specification ==

Exercise 1.3

The weight update rule in (1.3) has the nice interpretation that it moves in the direction of classifying $\mathbf{x}(t)$ correctly.

* (a) Show that $y(t) \; \mathbf{w}^T(t) \; \mathbf{x}(t) < 0$.

* (b) Show that $y(t+1) \; \mathbf{w}^T(t+1) \; \mathbf{x}(t) > y(t) \; \mathbf{w}^T(t) \; \mathbf{x}(t)$.

* (c) As far as classifying $\mathbf{x}(t)$ is concerned, arge that the move from $\mathbf{w}(t)$ to $\mathbf{w}(t+1)$ is a move "in the right direction."


== Hint ==

== Solution ==

=== (a) ===

$\mathbf{x}(t)$ is misclassified by $\mathbf{w}(t)$, which means $\mathbf{w}^T(t) \mathbf{x}(t)$ has the sign opposite to $y(t)$.  Therefore, $y(t) \mathbf{w}^T(t) \mathbf{x}(t) < 0$.


=== (b) ===

Eq.(1.3) is the update rule for $\mathbf{w}(t)$:

$$
\mathbf{w}(t+1) = \mathbf{w}(t) + y(t) \; \mathbf{x}(t)
$$

Take the transpose the Eq.(1.3) and multiply $y(t) \mathbf{x}(t)$ on both sides:

$$
y(t) \mathbf{w}^T(t+1) \; \mathbf{x}(t)
= y(t) \; \mathbf{w}^T(t) \; \mathbf{x}(t)
+ y^2(t) \; \lvert \mathbf{x}(t) \rvert^2
$$

Since $y^2(t) \; \lvert \mathbf{x}(t) \rvert^2 > 0$,


$$
y(t) \mathbf{w}^T(t+1) \; \mathbf{x}(t)
> y(t) \; \mathbf{w}^T(t) \; \mathbf{x}(t)
$$

=== (c) ===

Since

$$
y(t) \mathbf{w}^T(t) \mathbf{x}(t) < 0,
$$

$$
y(t) \mathbf{w}^T(t+1) \; \mathbf{x}(t)
> y(t) \; \mathbf{w}^T(t) \; \mathbf{x}(t)
$$

the update rule Eq.(1.3) increases $y(t) \mathbf{w}^T(t) \mathbf{x}(t)$ until it passes 0, where $\{\mathbf{x}(t), y(t)\}$ is no longer misclassified by $\mathbf{w}(t)$.


== Answer ==

== Discussion ==

== Related problems ==

== External links ==

== References ==

== Tags ==


[[Category:Problem]]

[[Category:ComputerScienceAndEngineering]]

[[Category:Algorithm]]

[[Category:LearningAlgorithm]]

[[Category:Solved]]
