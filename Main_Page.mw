== Introduction ==

Problemania is a group of enthusiasts enjoying solving mathematics, physics, and computing problems. We primarily work on problems found in university and graduate school level text books.

* [https://twitter.com/problemania Follow us on Twitter!]

Problemania.org is a web information system project inspired by well-known knowledge management systems [http://arxiv.org arXiv] and [http://wikipedia.org Wikipedia]. The aim is to eventually develop a systematically curated, comprehensive, open-access repository of scientific problems and solutions in quantitative disciplines such as physics, mathematics and computer science. The motivation is to build a knowledge archiving and distribution infrastructure for the persistent information contained in problems and solutions, namely the ''pedagogical application of conceptual knowledge'', which is ''complementary'' to factual/conceptual knowledge curated at Wikipedia and advanced research papers curated at arXiv, by leveraging the emerging novel Web technologies and the traditional approaches, which can be unnecessarily labor-intensive and repetitive.

== Recent problems ==

* [[How many different graphs can be formed with a given number of vertices]] (graph theory)
* [[Number of comparison operations done in a binary search]] (algorithm)

* [[Inhabited set is separator for the category of sets]] (category theory)

=== Problem of the day ===

* [[Inhabited set is separator for the category of sets]]
* [[Number of comparison operations done in a binary search]]
* [[Maximum speed of water wave]]

== Editors quick start ==

* We do support LaTeX, please feel free to format your writings using LaTeX. For example,
 <nowiki>$$\sum_{n=0}^\infty \frac{x^n}{n!}$$</nowiki>
will display as
$$\sum_{n=0}^\infty \frac{x^n}{n!}$$


== Quick links ==
* [[Special:AllPages|All pages]]
* [[Special:Categories|Categories]]
* [[Problem page template]]

== References ==
* [http://www.mediawiki.org/wiki/Help:Formatting MediaWiki formatting syntax help]
