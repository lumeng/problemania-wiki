
== Metadata ==
{{problem
|problem ID=201409172034-quicksort-with-mergesort-analysis
|name=quicksort with mergesort analysis
|book ID=sedgewick-aoa
|ISBN-13=978-0321905758
|page=25
|problem index=Exercise 1.17
|classification=[[:Category:Computer_Science|Computer Science]]`[[:Category:Data_Structures_and_Algorithms|Data Structures and Algorithms]]
}}

== Background ==

Quicksort is a typical introductory example of analysis of algorithms. Quicksort-with-mergesort, i.e. quicksort but falling back to mergesort for small subarrays, is an optimization of quicksort to achieve better asymptotic time complexity. Practicing analysis of algorithms for such an algorithm is a nice practice after studying the regular quicksort analysis.

[[book: sedgewick-aoa]] Theorem 1.3 (Quicksort analysis). Quicksort
uses, on average,

* $(N-1)/2$ partition stages,
* $2N(H_{N+1} -3/2) \approx 2 N \ln N - 1.846 N$ compares, and
* $(N+1)(H_{N+1} -3)/3 +1 \approx \frac{1}{3} N \ln N - 0.865 N$ exchanges.

== Prerequisite ==

Basics of data structures and algorithms.  Quick sort, merge sort.

== Involved concepts ==

* [http://en.wikipedia.org/wiki/Quicksort quicksort]
* [http://en.wikipedia.org/wiki/Merge_sort merge sort]
* [http://en.wikipedia.org/wiki/Analysis_of_algorithms analysis of algorithms]
* [http://en.wikipedia.org/wiki/Recurrence_relation recurrence relation]

== Problem specification ==


----
 private void quicksortWithMergesort(int[] a, int lo, int hi) {
     if (hi <= lo) return;
     if (hi - lo <= m) {
         insertionsort(a, lo, hi);
     } else {
         int i = lo-1, j = hi;
         int t, v = a[hi];
         while (true) {
             while (a[++i] < v) ;
             while (v < a[--j]) if (j == lo) break;
             if (i >= j) break;
             t = a[i]; a[i] = a[j]; a[j] = t;
         }
         t = a[i]; a[i] = a[hi]; a[hi] = t;
         quicksortWithMergesort(a, lo, i-1);
         quicksortWithMergesort(a, i+1, hi);
     }
 }
----
The total number of compares to sort N elements is described by the
recurrence

$$
C_N =
\left\{
  \begin{array}{ll}
    N + 1 + \frac{1}{N} \sum_{i=1}^N \big( C_{j-1} + C_{N-j} \big ) & \text{ for } N > M; \\
    \frac{1}{4} N (N-1) & \text{ for } N \leq M. 
  \end{array}
\right.
$$

== Hint ==

== Solution ==

Let $j = N - i + 1$,

$$
\sum_{i=1}^N C_{N-i} = \sum_{j=N}^1 C_{j-1} = \sum_{i=1}^N C_{i-1},
$$

hence

$$
C_N = N + 1 + \frac{2}{N} \sum_{i=1}^N C_{i-1}.
$$


For sufficiently large $N > M+1$:

$$
N C_N = N (N + 1) + 2 \sum_{i=1}^N C_{i-1},
(N-1) C_{N-1} = (N - 1) N  + 2 \sum_{i=1}^{N-1} C_{i-1}.
$$

Subtract the two equations, rearrange, and devide by $N(N+1)$ yields

$$
\frac{C_N}{N+1} = \frac{C_{N-1}}{N} + \frac{2}{N+1}.
$$

This recurrence "telescopes":


$$
\begin{aligned}
\frac{C_N}{N+1}
& = \frac{C_{N-1}}{N} + \frac{2}{N+1} \\
& = \frac{C_{N-2}}{N-1} + 2 \Big( \frac{1}{N} + \frac{1}{N+1} \Big ) \\
& \vdots \\
& = \frac{C_M}{M+1} + 2 \Big(\frac{1}{M+2} + \cdots + \frac{1}{N} + \frac{1}{N+1} \Big ) \\
& = \frac{C_M}{M+1} + 2 \big( H_{N+1} - H_{M+1} \big )
\end{aligned}
$$


Note for $N \leq M$, $C_N = \frac{1}{4} N (N-1)$, therefore

$$
\frac{C_N}{N+1} = \frac{M (M-1)}{4 (M+1)} + 2 \big( H_{N+1} - H_{M+1} \big )
$$

$$
C_N = \frac{M (M-1)}{4 (M+1)} (N+1) + 2 (N+1) \big( H_{N+1} - H_{M+1} \big )
$$

Now use the approximation

$$H_N = \ln N + 0.58$$

and only keep the leading order and second order terms:

$$
C_N = 2 N \ln N - \Big ( 2 \ln M - \frac{M (M-1)}{4 (M+1)} \Big) N + \mathcal{O}(\ln N)
$$


Special values of the coefficient:

$$
f(M) = \Big ( 2 \ln M - \frac{M (M-1)}{4 (M+1)} \Big)
$$

* When $M \approx 8.2$, $f(M)$ reaches [http://www.wolframalpha.com/input/?i=min+x(x-1)%2F(4(x%2B1))+-+2+ln+x minimum of approximately $f(8.2) \approx 2.6$].

* When $M \approx 18$, $f(M)$ reaches the critical point where [http://www.wolframalpha.com/input/?i=solve+-+%282+ln+x+-+x%28x-1%29%2F%284%28x%2B1%29%29%29+%3E+-+1.846 larger $M>18$ makes $C_N(M\gtrsim 18) \geq C_N(M=0)$].


=== TODO Write a program to experimentally verify the asymptotic behavior ===

== Answer ==

$$
C_N = 2 N \ln N - \Big ( 2 \ln M - \frac{M (M-1)}{4 (M+1)} \Big) N + \mathcal{O}(\ln N)
$$


== Discussion ==

== Related problems ==

== External links ==

== References ==

== Tags ==


[[Category:Problem]]

[[Category:ComputerScienceAndEngineering]]

[[Category:Algorithm]]

[[Category:SearchAlgorithm]]

[[Category:Solved]]
