
== Metadata ==
{{problem
|problem ID=201409301223-monty-hall-problem
|name=Monty Hall Problem
|classification=[[:Category:Mathematics|Mathematics]]`[[:Category:Probability|Probability]]
}}

== Background ==

== Prerequisite ==

== Involved concepts ==

== Problem specification ==

== Hint ==

== Solution ==

An explanation by words: with [https://en.wikipedia.org/wiki/Monty_Hall_problem#Standard_assumptions standard assumptions], in Bayes view, the odds that the contestant guessed correct, i.e. the car is behind No. 1, remains 1:3 after the show host reveals a goat-hiding gate. Thus, the odds that she guessed wrong are 2:3. And if she guessed wrong, the car must be behind the door she did not choose initially and neither opened by the host. So she should indeed switch, which will give her an odds of 2/3 of winning.

Solution by conditional probability and Bayes's theorem derivation:

Consider the events ''C1'', ''C2'' and ''C3'' indicating the car is behind respectively door 1,2 or 3. All these 3 events have probability 1/3.

The player picking door 1 is described by the event ''X1''. As the first choice of the player is independent of the position of the car, also the conditional probabilities are $P(Ci\;|X1)=1/3$, $i=1,2,3$.  The host opening door 3 is described by ''H3''. For this event it holds:

$$P(H3|C1,X1)=1/2$$
$$P(H3|C2,X1)=1$$
$$P(H3|C3,X1)=0$$

Then, if the player initially selects door 1, and the host opens door 3, the conditional probability of winning by switching is

$$
\begin{align}
P(C2|H3,X1) &= \frac{P(H3|C2,X1)P(C2|X1)}{P(H3|X1)} \\
&=\frac{P(H3|C2,X1)P(C2|X1)}{P(H3|C1,X1)P(C1|X1)+P(H3|C2,X1)P(C2|X1)+P(H3|C3,X1)P(C3|X1)}\\
&=\frac{P(H3|C2,X1)}{P(H3|C1,X1)+P(H3|C2,X1)+P(H3|C3,X1)} =\frac{1}{1/2+1+0} =\frac 23
\end{align}
$$

== Answer ==

== Discussion ==

== Related problems ==

== External links ==

== References ==

== Tags ==


[[Category:Problem]]

[[Category:Mathematics]]

[[Category:Probability]]

[[Category:Puzzle]]

[[Category:Solved]]
